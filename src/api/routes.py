"""
Main router with all routers included.
"""

from src.api import healthcheck
from src.api import request
from fastapi import APIRouter

API_ROUTER = APIRouter()

API_ROUTER.include_router(
    healthcheck.ROUTER, tags=["healthcheck"], prefix="/healthcheck"
)
API_ROUTER.include_router(request.ROUTER, tags=["Service"], prefix="/service")
