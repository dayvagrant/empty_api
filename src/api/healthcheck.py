from src.models.healthcheck import HealthcheckResult

from fastapi import APIRouter

ROUTER = APIRouter()


@ROUTER.get(
    "/",
)
def get_health_check() -> HealthcheckResult:
    """
    Healthcheck endpoint.
    """
    health_check = HealthcheckResult(is_alive=True)

    return health_check
