from fastapi import APIRouter, HTTPException, BackgroundTasks
from celery.result import AsyncResult
from loguru import logger

from src.models.request import Request as ModelRequest
from src.celery_app.tasks import something_pipeline
from src.celery_app.utils import check_task_status

# Создание экземпляра роутера FastAPI
ROUTER = APIRouter()


@ROUTER.post("/something/")
async def trigger_task(request: ModelRequest, background_tasks: BackgroundTasks):
    """
    Эндпоинт для запуска задачи через Celery.
    """
    logger.debug(f"Received request: {request}")

    # Запуск Celery задачи с использованием kwargs
    task = something_pipeline.apply_async(kwargs={"query": request.query})

    # Добавление задачи проверки статуса в фоновые задачи
    background_tasks.add_task(check_task_status, task.id)

    return {"status": "PENDING", "task_id": task.id}


@ROUTER.get("/task-status/{task_id}")
async def fetch_task_status(task_id: str):
    """
    Эндпоинт для получения статуса задачи Celery по её task_id.
    """
    task = AsyncResult(task_id)  # Получение объекта задачи по ID

    if task.state == "PENDING":
        return {"status": "Task is still pending..."}
    elif task.state == "SUCCESS":
        return task.result
    elif task.state == "FAILURE":
        raise HTTPException(
            status_code=500,
            detail=str(task.result.get("message", "Task failed without message")),
        )
    else:
        return {"status": f"Task is in state: {task.state}"}
