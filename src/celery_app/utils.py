import asyncio
from functools import wraps
from celery.result import AsyncResult
from loguru import logger


def async_to_sync(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        loop = asyncio.get_event_loop()
        if loop.is_closed():
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
        try:
            result = loop.run_until_complete(func(*args, **kwargs))
        finally:
            if loop.is_running():
                loop.close()
        return result

    return wrapper


async def check_task_status(task_id: str):
    task = AsyncResult(task_id)  # Получение задачи по ID

    # Ждем результат задачи с таймаутом (если результат нужен)
    result = task.get(timeout=60)  # Увеличенный таймаут ожидания, если требуется

    # Логируем результат или выполняем дополнительную обработку
    logger.info(f"Task {task_id} completed with result: {result}")
