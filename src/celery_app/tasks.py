from src.connections.broker import celery_app
from src.services.example_service import Function
from loguru import logger


@celery_app.task
def something_pipeline(**kwargs):
    try:
        response = Function.some_task(**kwargs)
        return {"status": "SUCCESS", "data": response}
    except Exception as e:
        error_response = {
            "status": "FAILURE",
            "message": str(e),
        }
        logger.error(error_response)
        return error_response
