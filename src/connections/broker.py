from celery import Celery

from src.core.settings import settings
from loguru import logger

REDIS_CONN_STR = "redis://{username}:{password}@{host}:{port}/{db}".format(
    username=settings.redis_connection.user,
    password=settings.redis_connection.password,
    host=settings.redis_connection.host,
    port=settings.redis_connection.port,
    db=settings.redis_connection.db,
)

celery_app = Celery(
    "tasks",
    broker=REDIS_CONN_STR,
    backend=REDIS_CONN_STR,
)
celery_app.autodiscover_tasks(force=True)
