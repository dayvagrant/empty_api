"""
Settings.
"""

import toml

from pydantic_settings import BaseSettings, SettingsConfigDict

with open("./pyproject.toml", "r") as pyproject:
    pyproject_content = pyproject.read()
    pyproject_parsed = toml.loads(pyproject_content)
    VERSION = pyproject_parsed["tool"]["poetry"]["version"]


class RedisConnection(BaseSettings):
    """
    Redis Connection Settings.
    """

    host: str
    port: str
    user: str
    password: str
    db: int

    model_config = SettingsConfigDict(
        env_file=".env",
        env_prefix="redis_",
        case_sensitive=False,
        env_file_encoding="utf-8",
        extra="ignore",
    )


class Settings(BaseSettings):
    """
    Application settings.
    """

    app_name: str = "Moex Search API"
    app_version: str = VERSION
    api_prefix: str = "/api"
    is_debug: bool = True

    redis_connection: RedisConnection = RedisConnection()

    model_config = SettingsConfigDict(
        env_file=".env", case_sensitive=False, env_file_encoding="utf-8", extra="ignore"
    )


settings = Settings()
