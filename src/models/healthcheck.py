"""
Model for endpoint.
"""

from pydantic import BaseModel
from src.core.utils import return_current_time


class HealthcheckResult(BaseModel):
    is_alive: bool
    date: str = return_current_time()
